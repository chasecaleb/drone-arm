#!/bin/bash
# Create connection with beaglebone over ethernet via DHCP, tunnel internet
# to it, automatically sync this directory in the background, and ssh in.

INET_IFACE=tun0
LOCAL_IFACE=enp0s25
LOCAL_IP=192.168.7.1
DEVICE_IP=192.168.7.100
DEVICE_USER=debian
echo -n "BBB password: "
read -rs DEVICE_PASS
echo ""

LOCAL_DIR="./"
REMOTE_DIR="/home/debian/drone-arm"
RSYNC_ARGS="-avr --delete --max-size=100m --exclude .git"
RSYNC_CMD="sshpass -p $DEVICE_PASS rsync $RSYNC_ARGS $LOCAL_DIR $DEVICE_USER@$DEVICE_IP:$REMOTE_DIR"

# Connection
sudo ip link set $LOCAL_IFACE up
sudo ip addr add $LOCAL_IP/24 dev $LOCAL_IFACE
killall dhcpd
sudo dhcpd -cf /etc/dhcpd.conf $LOCAL_IFACE

# Enable internet forwarding.
echo 1 | sudo tee /proc/sys/net/ipv4/ip_forward
# Setup iptables rules to forward connection to internet.
sudo iptables -t nat -A POSTROUTING -o $INET_IFACE -j MASQUERADE
sudo iptables -A FORWARD -i $LOCAL_IFACE -o $INET_IFACE -j ACCEPT
sudo iptables -A FORWARD -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT

# Make sure device is up.
echo "Waiting for ping..."
while ! ping -c1 $DEVICE_IP &> /dev/null; do sleep 1; done
echo "Ping success"

# Continously rsync on changes.
echo "Copying..."
STATUS=1
while [[ $STATUS -ne 0 ]]; do
    $RSYNC_CMD
    STATUS=$1
    sleep 1
done
echo ""

while inotifywait --recursive --event modify,close_write,move,create,delete $LOCAL_DIR; do
    echo "Copying..."
    $RSYNC_CMD
    echo ""
done
