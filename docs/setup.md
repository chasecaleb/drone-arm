# Setup
* Using `bone-debian-7.8-console-armhf-2015-03-01-2gb.img` on microsd
* Install debian packages (below)
* update kernel via `/opt/scripts/tools/update_kernel.sh`
    * may need `apt-get install lsb-release`
* [dtc](https://github.com/beagleboard/bb.org-overlays/blob/master/readme.md)
    * `./dtc_overlay.sh && ./install.sh` after cloning repo
* make virtual env using pyvenv (from python 3.4+)
* Install python packages in virtualenv
    * (NO) Adafruit_BBIO (spi for adc, setup uart for serial)
    * pyserial
    * spidev
    * evdev

# Dependencies: Debian packages
* kernel 3.8
* python (3.4+, may need to compile from source)
* (?) python3-dev
* (?) python3-pip
* xserver-xorg-input-evdev (for Logitech usb controller)
* usbutils (for lsusb, etc)
* ntpdate (set time with `sudo ntpdate pool.ntp.org`)
* wireless-tools
* firmware-realtek (for usb wifi adapter)

# SPI + Serial Notes
* `echo BB-SPIDEV0 > /sys/devices/bone_capemgr.?/slots`
* Beaglebone-ADXL345 connection: 17-CS, 18-SDA, 21-SDO, 22-SCL
* `echo BB-UART4 > /sys/devices/bone_capemgr.?/slots`
* UART4 = /dev/ttyO4, pins `P9_13` (TX) and `P9_11` (RX)

# Wifi
* (?) Usb wifi driver (http://elinux.org/RPi_edimax_EW-7811Un)
* setup
    * ifconfig wlan0 down
    * iwconfig wlan0 mode ad-hoc
    * ifconfig wlan0 up
    * iwconfig wlan0 channel 1 essid beaglebone
    * ifconfig wlan0 192.168.10.2 netmask 255.255.255.0
    * iwlist wlan0 scan
    * iwconfig wlan0 ap [mac addr from scan results]
        * mac addr should be/probably is/was 02:11:87:29:55:FE

# Sensor Notes
* **Sensitivity/noise:** at 1600 and 3200hz sample rates with max sensitivity (+/-
  2g), the LSB is always 0... so don't read that fast without a reason to. Also,
  noise increase by sqrt(2) per doubling of sample rate beyond 100hz. Sample
  rates <= 100hz have virtually equal noise levels. So for the sake of this
  project, sampling at 100hz is likely the best choice.
