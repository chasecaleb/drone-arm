import numpy
import collections
from csv_logger import CsvLogger


class DataProcessing:
    """ Perform some basic signal and also log raw sample readings to a csv file.
    """
    def __init__(self, axis, sample_rate, sample_count, max_hz):
        """ max_sample_count should be a power of 2 for efficiency, according
        to numpy docs. """
        self._logger = CsvLogger()
        self._axis = axis
        self._sample_rate = sample_rate
        self._sample_count = sample_count
        # Compute index for low pass
        fft_freqs = numpy.fft.rfftfreq(self._sample_count,
                                        d=1.0 / self._sample_rate)
        self._lpf_index = numpy.argmax(fft_freqs > max_hz) - 1
        self._samples = [collections.deque([0] * sample_count, sample_count)
                         for _ in range(axis)]

    def add_sample(self, sample):
        self._logger.write(sample)
        if self._axis == 1:
            self._samples[0].append(sample)
        else:
            for axis, value in enumerate(sample):
                self._samples[axis].append(value)

    def calculate(self):
        result = list()
        for i in range(self._axis):
            # Use window function to clean up
            windowed = self._samples[i] * numpy.hanning(len(self._samples[i]))
            # Do FFT
            w = numpy.abs(numpy.fft.rfft(windowed))
            fft_freqs = numpy.fft.rfftfreq(self._sample_count,
                                           d=1.0 / self._sample_rate)
            # Remove 0 hz (DC carrier frequency)
            w = w[1:]
            fft_freqs = fft_freqs[1:]
            # Low pass
            w = w[:self._lpf_index]
            fft_freqs = fft_freqs[:self._lpf_index]
            # Determine peak
            peak_index = numpy.argmax(w)
            peak_hz = fft_freqs[peak_index]

            # Ignore 0th element, since it corresponds to 0 hz/DC offset.
            result.append({'x': fft_freqs, 'y': w, 'peak': peak_hz})
            # Display as magnitude instead of raw
            # magnitude_scaled = 20 * numpy.log10(w)
            # result.append({'x': fft_freqs, 'y': magnitude_scaled,
                           # 'peak': peak_hz})

        return result
