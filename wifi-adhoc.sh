#!/bin/bash
DEV=wlp8s0
SSID=beaglebone
DEV_IP=192.168.10.2
CELL="02:11:87:22:CC:FD"

ip link set $DEV down
iwconfig $DEV essid $SSID mode ad-hoc
iwconfig $DEV channel 1
ip link set $DEV up
ifconfig $DEV $DEV_IP netmask 255.255.255.0
iwconfig $DEV ap $CELL
