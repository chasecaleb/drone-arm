import serial


class PololuServos:
    """ API for Pololu Maestro Servo Controller.
    * Note: make sure to call set_speed() and set_acceleration() if desired.
    * Documentation on Pololu Maestro Servo Controller:
    https://www.pololu.com/docs/0J40/all
    """
    def __init__(self, channels, min_pulse=825, max_pulse=2175,
                 degree_range=130, port='/dev/ttyO4', baud=115200):
        """ channels is a dict of {channel_number: is_reversed} values.
        """
        if type(channels) is not dict:
            raise TypeError
        self.channels = channels
        self.min_pulse = int(min_pulse)
        self.max_pulse = int(max_pulse)
        self.degree_range = degree_range
        self.serial = serial.Serial(port, baud, timeout=0.25)
        self.rotation = self.degree_range / 2

    @property
    def rotation(self):
        return self._rotation

    @rotation.setter
    def rotation(self, value):
        """ Expressed in degrees; must be between 0 and self.degree_range.
        * Uses self.min_pulse, self.max_pulse, and self.degree_range to
        determine actual value to send to servo.
        """
        if value < 0 or value > self.degree_range:
            raise ValueError
        self._rotation = value
        #print('rotation set to:', self._rotation)

        for channel, is_reversed in self.channels.items():
            d = self.degree_range - value if is_reversed else value
            pulse_diff = self.max_pulse - self.min_pulse
            mapped = 4 * (d * pulse_diff / self.degree_range + self.min_pulse)
            self._send_pulse(channel, int(mapped))

    @property
    def current_rotation(self):
        # Only need to check a single servo.
        channel, is_reversed = next(iter(self.channels.items()))
        cmd = [0xAA, 0x0C, 0x10, channel]
        self.serial.write(cmd)
        recv = self.serial.read(2)
        raw = recv[0] | (recv[1] << 8)
        min_scaled = 4 * self.min_pulse
        pulse_range = self.max_pulse - self.min_pulse
        value = self.degree_range * (raw - min_scaled) / (4 * pulse_range)
        self._current_rotation = (self.degree_range - value if is_reversed
                                  else value)
        return self._current_rotation

    @property
    def speed(self):
        if hasattr(self, '_speed'):
            return self._speed
        return None

    @speed.setter
    def speed(self, value):
        """ Expressed in 0.25us / 10ms (see Pololu documentiation).
        * 0 = no limit.
        * Higher value -> faster movement.
        * Example from docs: 140 = 3.5us / ms. Thus it takes 100ms to move from
        a pulse position of 1000us to 1350us.
        """
        if value < 0 or value > 255:
            raise ValueError
        self._speed = int(value)

        for channel, _ in self.channels.items():
            cmd = [0xAA, 0x0C, 0x07, channel, self._speed & 0x7F,
                   (self._speed >> 7) & 0x7F]
            self.serial.write(cmd)

    @property
    def acceleration(self):
        if hasattr(self, '_acceleration'):
            return self._acceleration
        return None

    @acceleration.setter
    def acceleration(self, value):
        """ Expressed in 0.25us / 10ms / 80ms... yeah (see Pololu docs).
        * 0 = no limit.
        * Higher value -> faster acceleration -> faster movement.
        * Applies to both start and end of movement.
        * Example from docs: at minimum value of 1, takes 3 seconds to move
        from 1,000us to 2,000us.
        """
        if value < 0 or value > 255:
            raise ValueError
        self._acceleration = int(value)

        for channel, _ in self.channels.items():
            cmd = [0xAA, 0x0C, 0x09, channel, self._acceleration & 0x7F,
                   (self._acceleration >> 7) & 0x7F]
            self.serial.write(cmd)

    def increment(self, amount):
        if self.current_rotation + amount > self.degree_range:
            self.rotation = self.degree_range
        elif self.current_rotation + amount < 0:
            self.rotation = 0
        else:
            self.rotation = self.current_rotation + amount

    def disable(self):
        """ Disable/turn off control of rotation until set again.
        In other words, allow servo to move freely.
        """
        for channel, _ in self.channels.items():
            cmd = [0xAA, 0x0C, 0x04, channel, 0, 0]
            self.serial.write(cmd)

    def stop(self):
        """ Stop servo at current_rotation. """
        try:
            self.rotation = self.current_rotation
        except ValueError:
            pass

    def _send_pulse(self, channel, pulse):
        cmd = [0xAA, 0x0C, 0x04, channel, pulse & 0x7F, (pulse >> 7) & 0x7F]
        self.serial.write(cmd)
