import unittest
from unittest import mock
import json
import server
import test_servo_control
import servo_control


class TestHandler(unittest.TestCase):
    def setUp(self):
        # Setup two joints, first having two servos and second a single servo.
        self.master_fd, slave_name = test_servo_control.virtual_serial()
        self.joints = list()
        self.joints.append(servo_control.ServoControl(
            {0: False, 1: True}, port=slave_name))
        self.joints.append(servo_control.ServoControl(
            {2: False}, port=slave_name))
        # Set joint parameters
        self.joints[0].rotation = 45
        self.joints[0].speed = 100
        self.joints[0].acceleration = 10
        self.joints[1].rotation = 90
        self.joints[1].speed = 200
        self.joints[1].acceleration = 20

    def test_error_msg_on_invalid_read_stream(self):
        m = json.dumps({}).encode()
        expected = json.dumps({'response': 'unknown_error'}).encode()
        self.assertEqual(expected, server.handle_data(self.joints, m))

    def test_ping(self):
        m = json.dumps({'command': 'ping'}).encode()
        expected = json.dumps({'response': 'alive'}).encode()
        self.assertEqual(expected, server.handle_data(self.joints, m))

    def test_get_status(self):
        m = json.dumps({'command': 'get_status'}).encode()
        joint_0 = {'rotation': 45, 'speed': 100, 'acceleration': 10}
        joint_1 = {'rotation': 90, 'speed': 200, 'acceleration': 20}
        expected = json.dumps(
            {'response': 'status', 'joints': [joint_0, joint_1]}).encode()
        self.assertEqual(expected, server.handle_data(self.joints, m))
        joint_0 = {'rotation': 45, 'speed': 100, 'acceleration': 10}
        joint_1 = {'rotation': 90, 'speed': 200, 'acceleration': 20}
        expected = json.dumps(
            {'response': 'status', 'joints': [joint_0, joint_1]}).encode()
        self.assertEqual(expected, server.handle_data(self.joints, m))

    def test_set_rotation(self):
        m = json.dumps({'command': 'set_rotation', 'joint': 0, 'value': 5}
                       ).encode()
        # No response should be sent
        self.assertEqual(None, server.handle_data(self.joints, m))
        # Should only set joint 0
        self.assertEqual(self.joints[0].rotation, 5)
        self.assertEqual(self.joints[1].rotation, 90)

    def test_increment(self):
        m = json.dumps(
            {'command': 'increment_rotation', 'joint': 0, 'value': 1.5}
        ).encode()
        self.assertEqual(None, server.handle_data(self.joints, m))
        self.assertEqual(self.joints[0].rotation, 46.5)

    def test_set_speed(self):
        m = json.dumps({'command': 'set_speed', 'value': 25}).encode()
        self.assertEqual(None, server.handle_data(self.joints, m))
        # Should set all joints
        self.assertEqual(self.joints[0].speed, 25)
        # Should set all joints
        self.assertEqual(self.joints[0].speed, 25)
        self.assertEqual(self.joints[1].speed, 25)

    def test_set_acceleration(self):
        m = json.dumps({'command': 'set_acceleration', 'value': 25}).encode()
        self.assertEqual(None, server.handle_data(self.joints, m))
        # Should set all joints
        self.assertEqual(self.joints[0].acceleration, 25)
        self.assertEqual(self.joints[1].acceleration, 25)

    def test_set_nonexistent_joint(self):
        m = json.dumps({'command': 'set_rotation', 'value': 0, 'joint':
                        99}).encode()
        expected = json.dumps({'response': 'unknown_error'}).encode()
        self.assertEqual(expected, server.handle_data(self.joints, m))

    def test_stop(self):
        self.joints[0].stop = mock.Mock()
        m = json.dumps({'command': 'stop', 'joint': 0}).encode()
        self.assertEqual(None, server.handle_data(self.joints, m))
        self.joints[0].stop.assert_called_once_with()

    def test_disable(self):
        self.joints[0].disable = mock.Mock()
        m = json.dumps({'command': 'disable', 'joint': 0}).encode()
        self.assertEqual(None, server.handle_data(self.joints, m))
        self.joints[0].disable.assert_called_once_with()
