import datetime
import csv
import os

class CsvLogger:
    """ Log data to a csv file.
    Usage: initialize, then call write() repeatedly.
    """
    def __init__(self, filename=None):
        if filename is None:
            directory = os.path.expanduser("~/drone_data")
            if not  os.path.exists(directory):
                os.makedirs(directory)
            timestamp = datetime.datetime.now().strftime("%Y%m%d.%H%M.%S")
            filename = os.path.join(directory, "drone_data." + timestamp)
        print("Logging samples to:", filename)
        self._log_file = open(filename, 'w', newline='')
        self._writer = csv.writer(self._log_file)

    
    def write(self, row):
        """ Writes row to file.
        See Python documentation of writerow() on csv.writer.
        """
        self._writer.writerow(row)
        # File buffering with csv.writer is a bit... questionable
        self._log_file.flush()
