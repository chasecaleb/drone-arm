#!/usr/bin/env python3
import spidev
import time


class ADXL345:
    """ ADXL345 accelerometer sensor using SPI interface.

    After initializing, call get_value() repeatedly to read. Other functions are primarily for
    configuration.  Refer to the ADXL345 datasheet for an overview on how the sensor works.
    """
    # REGISTER MASKS
    READ_MASK = 0x80
    MULTIREAD_MASK = 0x40
    # REGISTERS
    DEV_ID = 0x00
    BW_RATE = 0x2C      #  Set sample rate
    POWER_CTL = 0x2D	#  Allows measure start/stop
    DATA_FORMAT = 0x31  #  Sensitivity/range, output data format, etc
    READ_START = 0x32   #  6 bytes starting here are: X0, X1, Y0, Y1, Z0, Z1
    FIFO_CTL = 0x38
    FIFO_STATUS = 0x39

    def __init__(self, bus=1, dev=0, rate=100, sensitivity=2):
        self.spi = spidev.SpiDev()
        self.spi.open(bus, dev)
        self.spi.mode = 3
        self.spi.max_speed_hz = 4000000
        self.set_enable(True)
        self.set_fifo(True)
        self.set_rate(rate)
        self.set_sensitivity(sensitivity)

    def set_fifo(self, enable, interrupt_threshold=8):
        if enable:
            # 0b10 is stream (drop old on overflow), 0b01 is fifo (drop new)
            bits = 0b10 << 6 | (interrupt_threshold & 31)
        else:
            bits = 0
        self.spi.xfer2([self.FIFO_CTL, bits])

    def get_fifo_count(self):
        """ Check how many samples are currently stored in FIFO buffer of sensor.
        """
        resp = self.spi.xfer2([self.FIFO_STATUS | self.READ_MASK, 0])
        # Return only 5 LSB's
        return (resp[0] << 8 | resp[1]) & 63

    def get_value(self):
        """ Call this to get next reading.  Since a FIFO buffer is used on the sensor timing does
        not have to perfect, so long as it is called frequently enough to keep up with the buffer
        (see datasheet). If buffer contains a new value, that will be returned. If not, this
        function blocks until next sample is read into buffer, then returns it.
        """
        count = self.get_fifo_count()
        while count == 0:
            count = self.get_fifo_count()

        bits = self.READ_START | self.READ_MASK | self.MULTIREAD_MASK
        raw = self.spi.xfer2([bits,0,0,0,0,0,0])
        # First byte is... junk of some sort. Leftover from last message?
        raw = raw[1:]
        usleep = lambda x: time.sleep(x/1000000.0)
        # According to data sheet, time between reads must be >= 5us (including
        # transmission time)
        usleep(3.75)

        x = self._convert(raw[0], raw[1])
        y = self._convert(raw[2], raw[3])
        z = self._convert(raw[4], raw[5])
        return (x, y, z)

    def get_dev_id(self):
        """ This should return 0xE5 (229) if working properly. """
        bits = self.DEV_ID | self.READ_MASK
        return self.spi.xfer2([bits, 0])[1]

    def set_rate(self, hertz):
        """ Set sample rate in hertz.
        Valid options are: 6.25, 12.5, 25, 50, 100, 200, 400, 800, 1600, 3200.
        """
        if hertz == 6.25:
            bits = 0b0110
        elif hertz == 12.5:
            bits = 0b0111
        elif hertz == 25:
            bits = 0b1000
        elif hertz == 50:
            bits = 0b1001
        elif hertz == 100:
            bits = 0b1010
        elif hertz == 200:
            bits = 0b1011
        elif hertz == 400:
            bits = 0b1100
        elif hertz == 800:
            bits = 0b1101
        elif hertz == 1600:
            bits = 0b1110
        elif hertz == 3200:
            bits = 0b1111
        else:
            raise ValueError
        self.spi.xfer2([self.BW_RATE, bits])

    def set_sensitivity(self, value):
        """ Valid settings are 2, 4, 8, and 16 (for range of +/- value g's). """
        if value not in (2, 4, 8, 16):
            raise ValueError
        elif value == 2:
            bits = 0b00
        elif value == 4:
            bits = 0b01
        elif value == 8:
            bits = 0b10
        elif value == 16:
            bits = 0b11
        self.spi.xfer2([self.DATA_FORMAT, bits])

    def set_enable(self, turn_on):
        value = 1 << 3 if turn_on else 0
        self.spi.xfer2([self.POWER_CTL, value])

    def _convert(self, lsb, msb):
        """ Convert the gravity data returned by the ADXL to meaningful values """
        value = (msb << 8) | lsb
        if value & 0x8000:
            value = -value ^ 0xFFFF
        return value


# If this file is run directly (ie "python ./adxl345.py"): read/print accelerometer values in a loop.
# This is more or less just for verifying that the sensor and code are working.
if __name__ == '__main__':
    a = ADXL345(rate=100, sensitivity=2)
    print("dev id:", hex(a.get_dev_id()))

    while True:
        x, y, z = a.get_value()
        print("X: %10s Y: %10s Z: %10s" % (x, y, z))
        time.sleep(0.05)
