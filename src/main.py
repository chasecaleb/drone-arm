#!/usr/bin/env python3
""" Main program start point for both client and server. Run with no arguments.
"""
import asyncio
try:
    import control_client
    import data_client
except ImportError:
    print("WARNING: ImportError caught on client files. This is okay if running as server.")
try:
    import control_server
    import data_server
except ImportError:
    print("WARNING: ImportError caught on server files. This is okay if running as client.")


if __name__ == '__main__':
    role = input("[s]erver or [c]lient: ")
    if role in ['s', 'S']:
        print("Serving...")
        control_server.main()
        data_server.main()
    elif role in ['c', 'C', 'client']:
        ip = input("IP of server: ")
        print("Starting client...")
        control_client.main(ip)
        data_client.main(ip)
    else:
        print("Invalid input")
        raise ValueError

    loop = asyncio.get_event_loop()
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        loop.close()
