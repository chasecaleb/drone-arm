import unittest
from unittest import mock
import os
import pty
import select
import servo_control


def virtual_serial():
    master_fd, slave_fd = pty.openpty()
    slave_name = os.ttyname(slave_fd)
    return master_fd, slave_name


def read_serial(fd, length=1000):
    r, _, _ = select.select([fd], [], [], 0)
    if fd in r:
        raw = os.read(fd, 1000)
        return [int(x) for x in raw]


class TestServoControl(unittest.TestCase):
    def setUp(self):
        self.master_fd, slave_name = virtual_serial()
        self.mc = servo_control.ServoControl({0: False}, port=slave_name)
        # Clear buffer since rotation is set to default value in init
        read_serial(self.master_fd)
        # Because there's no physical servo to return a value
        self.mc.serial.read = mock.MagicMock(
            return_value=self.mock_read_current())

    def tearDown(self):
        os.close(self.master_fd)

    # FIXME i don't even know
    def mock_read_current(self):
        deg = self.mc.rotation
        diff = self.mc.max_pulse - self.mc.min_pulse
        value = 4 * (deg * diff / self.mc.degree_range + self.mc.min_pulse)
        value = int(value)
        low = value & 0x8F
        high = value >> 8
        return [low, high]

    def move_cmd(self, channel, pulse):
        return [0xAA, 0x0C, 0x04, channel, pulse & 0x7F,
                (pulse >> 7) & 0x7F]

    def test_exception_on_invalid_channels_param_in_constructor(self):
        with self.assertRaises(TypeError):
            servo_control.ServoControl(1)

    def test_exception_on_negative_degrees(self):
        with self.assertRaises(ValueError):
            self.mc.rotation = -1

    def test_exception_on_too_large_of_degrees(self):
        with self.assertRaises(ValueError):
            self.mc.rotation = 999

    def test_get_rotation_before_initialization(self):
        self.assertEqual(65, self.mc.rotation)

    def test_get_rotation(self):
        self.mc.rotation = 100
        self.assertEqual(100, self.mc.rotation)

    def test_set_rotation_to_zero(self):
        self.mc.rotation = 0
        self.assertListEqual(self.move_cmd(0, 825 * 4),
                             read_serial(self.master_fd))

    def test_set_rotation_to_90(self):
        self.mc.rotation = 90
        self.assertListEqual(self.move_cmd(0, 7038),
                             read_serial(self.master_fd))

    def test_set_rotation_to_130(self):
        self.mc.rotation = 130
        self.assertListEqual(self.move_cmd(0, 2175 * 4),
                             read_serial(self.master_fd))

    def test_set_rotation_as_a_float(self):
        # Ensure it doesn't merely round the rotation to an int and send that.
        self.mc.rotation = 90.3
        self.assertListEqual(self.move_cmd(0, 7050),
                             read_serial(self.master_fd))

    def test_increment_by_float_less_than_one(self):
        self.mc.rotation = 90.3
        self.assertListEqual(self.move_cmd(0, 7050),
                             read_serial(self.master_fd))
        self.mc.increment(-0.3)  # Back to 90.0 degrees
        self.assertListEqual(self.move_cmd(0, 7038),
                             read_serial(self.master_fd))

    def test_two_motors_are_properly_reversed_at_130(self):
        self.mc.channels = {0: False, 1: True}
        self.mc.rotation = 130
        expected = self.move_cmd(0, 2175 * 4)
        expected += self.move_cmd(1, 825 * 4)
        self.assertListEqual(expected, read_serial(self.master_fd))

    def test_two_motors_are_properly_reversed_at_10(self):
        self.mc.channels = {0: False, 1: True}
        self.mc.rotation = 10
        expected = self.move_cmd(0, 3715) + self.move_cmd(1, 8284)
        self.assertListEqual(expected, read_serial(self.master_fd))

    def test_get_speed_before_initialization(self):
        self.assertEqual(None, self.mc.speed)

    def test_get_speed(self):
        self.mc.speed = 100
        self.assertEqual(100, self.mc.speed)

    def test_set_speed_out_of_range_raises_error(self):
        with self.assertRaises(ValueError):
            self.mc.speed = -1
        with self.assertRaises(ValueError):
            self.mc.speed = 256

    def test_set_speed(self):
        self.mc.speed = 150
        expected = [0xaa, 0x0c, 0x07, 0x00, 150 & 0x7f, (150 >> 7) & 0x7f]
        self.assertListEqual(expected, read_serial(self.master_fd))

    def test_acceleration_before_initilization(self):
        self.assertEqual(None, self.mc.acceleration)

    def test_set_acceleration_out_of_range_raises_error(self):
        with self.assertRaises(ValueError):
            self.mc.acceleration = -1
        with self.assertRaises(ValueError):
            self.mc.acceleration = 256

    def test_get_acceleration(self):
        self.mc.acceleration = 140
        self.assertEqual(140, self.mc.acceleration)

    def test_set_acceleration(self):
        self.mc.acceleration = 140
        expected = [0xaa, 0x0c, 0x09, 0x00, 140 & 0x7f, (140 >> 7) & 0x7f]
        self.assertListEqual(expected, read_serial(self.master_fd))

    def test_increment_positive_value(self):
        self.mc.rotation = 50
        self.mc.increment(1)
        self.assertEqual(51, self.mc.rotation)

    def test_increment_negative_value(self):
        self.mc.rotation = 50
        self.mc.increment(-1)
        self.assertEqual(49, self.mc.rotation)

    def test_increment_out_of_range_sets_to_min(self):
        self.mc.rotation = 3
        self.mc.increment(-10)
        self.assertEqual(0, self.mc.rotation)

    def test_increment_out_of_range_sets_to_max(self):
        self.mc.rotation = self.mc.degree_range - 5
        self.mc.increment(10)
        self.assertEqual(self.mc.degree_range, self.mc.rotation)

    def test_set_speed_as_float(self):
        self.mc.speed = 30.3
        self.assertEqual(30, self.mc.speed)

    def test_current_rotation(self):
        expected_send = [0xAA, 0x0C, 0x10, 0x00]
        location = 89.988888889  # 90 to nearest bit
        os.write(self.master_fd, chr(7038 & 0x7F).encode())
        os.write(self.master_fd, chr(7038 >> 8).encode())
        self.assertAlmostEqual(location, self.mc.current_rotation, places=5)
        self.assertEqual(expected_send, read_serial(self.master_fd))

    def test_current_rotation_two_servos(self):
        self.mc.channels = {0: False, 1: True}
        expected_send = [0xAA, 0x0C, 0x10, 0x00]
        location = 89.988888889  # 90 to nearest bit
        os.write(self.master_fd, chr(7038 & 0x7F).encode())
        os.write(self.master_fd, chr(7038 >> 8).encode())
        self.assertAlmostEqual(location, self.mc.current_rotation, places=5)
        self.assertEqual(expected_send, read_serial(self.master_fd))

    def test_current_rotation_two_servos_swapped(self):
        self.mc.channels = {0: True, 1: False}
        expected_send = [0xAA, 0x0C, 0x10, 0x00]
        location = 89.988888889  # 90 to nearest bit
        os.write(self.master_fd, chr(4962 & 0x7F).encode())
        os.write(self.master_fd, chr(4962 >> 8).encode())
        self.assertAlmostEqual(location, self.mc.current_rotation, places=5)
        self.assertEqual(expected_send, read_serial(self.master_fd))

    def test_disable(self):
        expected = [0xAA, 0x0C, 0x04, 0x00, 0x00, 0x00]
        self.mc.disable()
        self.assertEqual(expected, read_serial(self.master_fd))

    def test_stop(self):
        mock_current = mock.PropertyMock(return_value=7)
        with mock.patch('servo_control.ServoControl.current_rotation',
                        mock_current):
            self.mc.stop()
        self.assertEqual(7, self.mc.rotation)
