# Introduction
Code and documentation for the "Utilization of Drones for Precise Sensor/Payload Deployment and
Environmental Interaction" research project. Or in other words: I designed and 3D printed a robotic
arm, stuck an accelerometer on the end, and then attached the whole thing to a drone in order to
measure vibrations on infrastructure such as bridges and railroad tracks.

# Physical Design
Look in the `stl` folder for STL files to 3D print. Look in the `solidworks` folder for the
Solidworks files. The most recent pieces were printed using PLA on my own custom made 3D printer,
but the school's 3D printers would work every bit as well (or better) than that. Towards the end of
the project I attempted to reprint them using polycarbonate filament in order to increase the
strength, but was not able to get my printer to cooperate with polycarbonate thus far.

Additionally, a rendering of the entire design with a few annotations is included in the `docs` folder.

# Electronics
A rough bill of materials (excluding obvious things such as wire) is included in the `docs`
folder. Instead of explaining how each part works, look up their datasheets. Everything is pretty
straightforward in this regard. On the server side, the beaglebone reads the accelerometer and
controls the motors using the Pololu servo controller board. On the client side, I used a Logitech
F310 USB video game controller as a remote control for the motors.

# Code
Each individual .py file should be more than adequately documented with comments to explain what's
going on. In order to run the program, call `main.py` on both the server (the drone side) and the
client (laptop).

While familiarizing yourself with the code, keep in mind that there are two more or less indepedent
components: the control portion and the data portion. Each of these can also be broken down into
separate server and client parts. For the control portion, start out with `control_client.py` and
`control_server.py`, then dig into whichever modules are used by them. For the data portion, start
out with `data_client.py` and `data_server.py`.

The connect.sh and wifi-adhoc.sh scripts were for my convenience when doing development on
Linux. Look through them if you use Linux and are interested, otherwise feel free to ignore them.

Also refer to `docs/setup.md` in regards to setting up the Beaglebone Black. Fair warning: this file
was intended to be notes for my own reference, so it's a bit cryptic and may contain a few things
that are inaccurate or out of date.