import asyncio
import json
import matplotlib.animation
import matplotlib.pyplot as plt
from data_processing import DataProcessing


class DataClient:
    """ Handles client side of data communication (that is, sensor reading).

    WARNING: the graphing code is a major kludge and, while it works for me, most likely won't work
    for you. I would strongly recommend tearing it out and rewriting with whatever Python graphing
    library you prefer. I was attempting to make matplotlib update at a decent speed with real time
    data, but it didn't go so well. Matplotlib is a pain to work with, unless you're used to Matlib.
    """
    AXES_LABELS = ['X', 'Y', 'Z']
    AXES_COLORS = ['red', 'green', 'blue']
    SAMPLE_RATE = 100
    SAMPLE_COUNT = 2**9
    Y_MAX = 2**9 * 128
    # Note: Nyquists theorem says max FFT frequency is 1/2 of sample rate
    MAX_HZ = 10
    FRAMERATE = 1 / 60

    def __init__(self, host='localhost', port='55101', loop=None):
        self.host = host
        self.port = port
        if loop is not None:
            self._loop = loop
        else:
            self._loop = asyncio.get_event_loop()
        self._server_poll_interval = 0.1
        self._processing = DataProcessing(3, self.SAMPLE_RATE,
                                          self.SAMPLE_COUNT, self.MAX_HZ)

        # Plot config
        self._figure, axes = plt.subplots(3, sharex=True)
        self._plots = list()
        # Empty initial data for plot
        empty_xs = list(range(self.SAMPLE_COUNT))
        empty_ys = [0] * self.SAMPLE_COUNT
        for i in range(3):
            d = dict()
            d['axis'] = axes[i]
            d['axis'].set_title(self.AXES_LABELS[i])
            d['axis'].set_ylim(0, self.Y_MAX)
            d['axis'].set_xlim(0, self.MAX_HZ)
            d['line'] = axes[i].plot(empty_xs, empty_ys,
                                     color=self.AXES_COLORS[i])[0]
            d['peak_line'] = axes[i].axvline(0, color='black', linestyle='--',
                                             linewidth=2)
            self._plots.append(d)

    def start(self):
        self._loop.create_task(self._send({'command': 'clear_data'}))
        self._loop.call_later(0.1, self._poll_server)

        self._plot_anim = matplotlib.animation.FuncAnimation(
            self._figure, self._update_plot, init_func=self._clear_plot,
            blit=True)
        plt.show(block=False)
        plt.pause(0.1)
        self._loop.call_soon(self._render_plot)

        try:
            self._loop.run_forever()
        except KeyboardInterrupt:
            pass
        self._loop.close()

    def _poll_server(self):
        self._loop.create_task(self._send({'command': 'get_data'}))
        self._loop.call_later(self._server_poll_interval, self._poll_server)

    def _render_plot(self):
        self._loop.call_later(self.FRAMERATE, self._render_plot)
        self._plot_anim._step()
        plt._pylab_helpers.Gcf.get_active().canvas.flush_events()

    def _clear_plot(self):
        for plot in self._plots:
            plot['line'].set_ydata([0] * self.SAMPLE_COUNT)
            plot['peak_line'].set_xdata([0, 0])
        return self._plot_return_gen()

    def _plot_return_gen(self):
        ret_val = [i['line'] for i in self._plots]
        ret_val += [i['peak_line'] for i in self._plots]
        return ret_val

    def _update_plot(self, _):
        data = self._processing.calculate()
        for axis, axis_data in zip(self._plots, data):
            axis['line'].set_data(axis_data['x'], axis_data['y'])
            axis['peak_line'].set_xdata([axis_data['peak'],
                                         axis_data['peak']])
            # TODO: figure out a hack to make this work with animation/blitting
            # axis['axis'].set_title('Max: %.3f hz' % axis_data['peak'],
            #                        loc='right')
        print("%10.3f %10.3f %10.3f" %
              (data[0]['peak'], data[1]['peak'], data[2]['peak']))
        return self._plot_return_gen()

    @asyncio.coroutine
    def _send(self, msg):
        try:
            r, w = yield from asyncio.open_connection(self.host, self.port)
        except ConnectionRefusedError:
            print("Connection failed, is server running?")
            print("")
            raise ConnectionRefusedError
        w.write(json.dumps(msg).encode())
        w.write_eof()
        recv = yield from r.read()
        if recv is not b'':
            recv_json = json.loads(recv.decode())
            if 'data' in recv_json:
                for sample in recv_json['data']:
                    self._processing.add_sample(sample)


def main(server_ip='192.168.7.100'):
    client = DataClient(host=server_ip)
    client.start()

if __name__ == '__main__':
    main()
