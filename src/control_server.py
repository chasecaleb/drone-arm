#!/usr/bin/env python3
import json
import asyncio
import functools
import pololu_servos

""" Server half of motor control. 
"""


@asyncio.coroutine
def connection_handler(joints, read_stream, write_stream):  # pragma: no cover
    data = yield from read_stream.read(2048)
    resp = handle_data(joints, data)
    if resp:
        write_stream.write(resp)
        write_stream.write_eof()


def handle_data(joints, data):
    send_resp = True
    resp = dict()
    try:
        msg = json.loads(data.decode())

        if msg['command'] == 'ping':
            resp['response'] = 'alive'
        elif msg['command'] == 'get_status':
            resp['response'] = 'status'
            resp['joints'] = list()
            for j in joints:
                val = {'rotation': j.rotation, 'speed': j.speed,
                       'acceleration': j.acceleration}
                resp['joints'].append(val)
        elif msg['command'] == 'set_rotation':
            n = msg['joint']
            joints[n].rotation = msg['value']
            send_resp = False
        elif msg['command'] == 'increment_rotation':
            # TODO: consider sending response with new rotation
            n = msg['joint']
            joints[n].increment(msg['value'])
            send_resp = False
        elif msg['command'] == 'set_speed':
            for j in joints:
                j.speed = msg['value']
            send_resp = False
        elif msg['command'] == 'set_acceleration':
            for j in joints:
                j.acceleration = msg['value']
            send_resp = False
        elif msg['command'] == 'stop':
            n = msg['joint']
            joints[n].stop()
            send_resp = False
        elif msg['command'] == 'disable':
            n = msg['joint']
            joints[n].disable()
            send_resp = False
    except (KeyError, TypeError, AttributeError, IndexError):
        pass

    if send_resp and resp == dict():
        resp['response'] = 'unknown_error'

    if send_resp:
        return json.dumps(resp).encode()
    else:
        return None


def main(port=55100):
    joints = list()
    joints.append(pololu_servos.PololuServos({0: True, 1: False}))
    joints.append(pololu_servos.PololuServos({2: False}))

    loop = asyncio.get_event_loop()
    handler = functools.partial(connection_handler, joints)
    coro = asyncio.start_server(handler, port=port)
    loop.run_until_complete(coro)


if __name__ == '__main__':  # pragma: no cover
    main()
    loop = asyncio.get_event_loop()
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass
    loop.close()
