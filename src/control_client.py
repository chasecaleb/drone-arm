import evdev
import asyncio
import json


class ControlClient:
    """ Client side: control motors (arm joints) using a USB game controller.

    For my developerment, I used a Logitech F310. Other controllers may work, I have no idea. If
    not, this should be easy enough to adapt.
    """
    def __init__(self, joints, input_dev, host='localhost', port='55100',
                 deadzone=0.15, refresh_rate=(1 / 30), max_speed=75):
        self.joints = joints
        self.controller = self.get_controller(input_dev)
        self.host = host
        self.port = port
        self.loop = asyncio.get_event_loop()

        self.deadzone = deadzone
        self.refresh_rate = refresh_rate
        self.max_speed = max_speed
        self.increment_amounts = dict()

        # This may or may not help a (moderate) amount with stability
        msg = {'command': 'set_acceleration', 'value': 40}
        self.loop.create_task(self.send(msg))

        self.loop.add_reader(self.controller.fd, self.handle)
        self.loop.call_soon(self.update_server)

    def get_controller(self, name):
        devs = [evdev.InputDevice(fn) for fn in evdev.list_devices()]
        for d in devs:
            if d.name == name:
                return d
        else:
            print("Controller not found, is it plugged in?")
            print("")
            raise ValueError

    def handle(self):
        event = self.controller.read_one()
        if event.type == 3:
            return self._handle_joystick(event)
        elif event.type == 1:
            return self._handle_button(event)

    def _handle_button(self, event):
        if event.code == 314 and event.value == 1:
            for j in self.joints.values():
                msg = {'command': 'disable', 'joint': j}
                self.loop.create_task(self.send(msg))

    def _handle_joystick(self, event):
        if event.code not in self.joints:
            return

        joint = self.joints[event.code]
        range_info = self.controller.capabilities()[3][event.code][1]
        value = event.value / range_info.max
        # Don't move if within deadzone
        if abs(value) < self.deadzone:
            value = 0
        # Scale so that edge of deadzone is 0 yet range remains -1.0 to 1.0
        elif value > 0:
            value -= self.deadzone
        else:
            value += self.deadzone
        value /= (1 - self.deadzone)

        self.increment_amounts[joint] = [value, True]

    def update_server(self):
        self.loop.call_later(self.refresh_rate, self.update_server)
        for joint, [value, do_update] in self.increment_amounts.items():
            if not do_update:
                continue

            if value == 0:
                self.increment_amounts[joint][1] = False
                msg = {'command': 'stop', 'joint': joint}
                self.loop.create_task(self.send(msg))
                continue

            speed = int(self.max_speed * abs(value)**(3 / 2))
            if speed < 1:
                speed = 1
            speed_msg = {'command': 'set_speed', 'value': speed}
            self.loop.create_task(self.send(speed_msg))

            accel_msg = {'command': 'set_acceleration', 'value': speed / 2}
            self.loop.create_task(self.send(accel_msg))

            if value > 0:
                value = 130
            else:
                value = 0
            msg = {'command': 'set_rotation', 'joint': joint,
                   'value': value}
            self.loop.create_task(self.send(msg))

    @asyncio.coroutine
    def send(self, msg):
        #print('sending:', msg)
        try:
            r, w = yield from asyncio.open_connection(self.host, self.port)
        except ConnectionRefusedError:
            print("Connection failed, is server running?")
            print("")
            raise ConnectionRefusedError
        w.write(json.dumps(msg).encode())
        w.write_eof()
        recv = yield from r.read()
        if recv is not b'':
            print('response:', json.loads(recv.decode()))


def main(server_ip='localhost', autorun=True):
    joints = dict()
    # Map of controller axis (key): servo joint (value)
    joints[1] = 0
    joints[4] = 1
    ctrl = ControlClient(joints, 'Logitech Gamepad F310', host=server_ip)


if __name__ == '__main__':
    main()
    loop = asyncio.get_event_loop()
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass
    loop.close()
