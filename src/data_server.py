#!/usr/bin/env python3
import asyncio
import functools
import json
import collections
import adxl345

""" Server side of data communication.
Polls accelerometer and sends readings to client.
"""


@asyncio.coroutine
def connection_handler(adxl, sensor_data, read_stream, write_stream):
    stream_data = yield from read_stream.read(2048)
    resp = handle_data(adxl, stream_data, sensor_data)
    if resp:
        write_stream.write(resp)
        write_stream.write_eof()


def handle_data(adxl, stream_data, sensor_data):
    resp = dict()
    try:
        msg = json.loads(stream_data.decode())
        if msg['command'] == 'get_data':
            resp['data'] = list()
            resp['length'] = len(sensor_data)
            while len(sensor_data) > 0:
                resp['data'].append(sensor_data.popleft())
        elif msg['command'] == 'clear_data':
            sensor_data.clear()
    except (KeyError, TypeError):
        pass

    if resp == dict():
        resp['response'] = 'unknown_error'
    else:
        return json.dumps(resp).encode()


def poll_adxl(loop, adxl, sensor_data):
    count = adxl.get_fifo_count()
    while count > 0:
        sample = adxl.get_value()
        sensor_data.append(sample)
        count -= 1

    loop.call_later(0.02, poll_adxl, loop, adxl, sensor_data)


def main(port=55101):
    loop = asyncio.get_event_loop()

    adxl = adxl345.ADXL345(rate=100, sensitivity=2)
    sensor_data = collections.deque()
    loop.call_soon(poll_adxl, loop, adxl, sensor_data)

    handler = functools.partial(connection_handler, adxl, sensor_data)
    coro = asyncio.start_server(handler, port=port)
    loop.run_until_complete(coro)


if __name__ == '__main__':
    main()
    loop = asyncio.get_event_loop()
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass
    loop.close()
